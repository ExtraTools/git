## Installing and using git

This session is related to best pratices in your project. You are going to learn 
how to use the full power of GitLab (the same may apply for github).This lab is 
very important since it will help you manage your project and keep track
of the progress. So far, you know what is git and you have a flavor of the basic 
git command. 

#### Preliminaries

1. If you have not done it yet, create an account on gitlab.com. 
1. Depending on your machine this is the way to install git:
    - linux ubuntu: apt-get install git vim
    - mac osx: https://git-scm.com/
    - windows: https://git-for-windows.github.io/
1. If you have a windows machine, install [SourceTree](https://www.sourcetreeapp.com/).
for other operating system it is not mandatory For all the version git can be used 
via a command line interface. Note that for
the windows version, a bash is specifically install with `vim`. You can
increase your git experience by using a GUI but it is not mandatory. 
there are a lot of them available for different platform. There is a list 
[here](https://git-scm.com/downloads/guis).  A GUI help visializing the state 
of your repository and easily interact with it.

Please carefully follow the instalation instruction for both software. If `sourcetree`
prompt you with an installation of another software just say yes. 



#### Step 0

Create a repository on your gitlab account, name it `myfirstgit`. This is done online 
through the web interface of `gitlab`. Create a readme file online. ___Do not follow
the indications prvided by gitlab at least for now___.

Clone you repository using the command line or `sourcetree`.  To do so, you can add 
a repository from URL. Use the terminal if you feel more confortable with it. 


#### Step 1

In `sourcetree` open the command line and follw the tutorial [here](https://gitlab.com/m3206/TD/blob/master/GIT.md) 
starting from exercice 1.



